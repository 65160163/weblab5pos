import { ref } from 'vue';
import type { VForm } from 'vuetify/components';

export const refForm = ref<VForm | null>(null);
